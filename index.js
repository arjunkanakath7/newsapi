const firstname = document.getElementById('name')
const firsttitle = document.getElementById('titles')
const firstdescription = document.getElementById('description')
const firstimage = document.getElementById('image')
const firstcontent = document.getElementById('content')
const firstauthor = document.getElementById('author')
const firstpublishedAt = document.getElementById('publishedAt')

fetch('https://newsapi.org/v2/everything?domains=wsj.com&apiKey=40e11e4e19054f15905f6c1a593c1054')

.then((response) => response.json())
.then(news => {
    const maintitle = news.articles[1]
    firsttitle.innerHTML = maintitle.title
})
.catch(error => {
    console.log(error)
})